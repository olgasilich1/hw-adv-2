const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const minifyjs = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');


const paths = {
    scss: './src/scss/**/*.scss',
    js: './src/js/**/*.js',
    img:  './src/img/**/*.{jpg,png,webp,gif,svg}'
}


const cleanDist = () => {
    return gulp.src('dist', {read: false, allowEmpty: true})
        .pipe(clean());
};

const css = () => {
    return gulp.src(paths.scss)
                .pipe(sass())
                .pipe(autoprefixer({
                    cascade: false
                }))
                .pipe(cleanCSS({compatibility: 'ie8'}))
                .pipe(concat('styles.min.css'))
                .pipe(gulp.dest('./dist/css/'))
}

const js = () => {
    return gulp.src(paths.js)
                .pipe(concat('scripts.min.js'))
                .pipe(minifyjs())
                .pipe(uglify())
                .pipe(gulp.dest('./dist/js/'))
}

const imp = () => {
    return gulp.src(paths.img)
                .pipe(imagemin())
                .pipe(gulp.dest('./dist/img/'))
}

const watch = () => {
    gulp.watch(paths.scss, css).on('change',browserSync.reload)
    gulp.watch(paths.js, js).on('change', browserSync.reload)
    gulp.watch(paths.img, img).on('change', browserSync.reload)
    gulp.watch('index.html').on('change', browserSync.reload)
    browserSync.init({
        server: {
            baseDir: './',
        },
    })

}

gulp.task('dev', watch)
gulp.task('build', gulp.series(cleanDist, gulp.parallel(css, js)))